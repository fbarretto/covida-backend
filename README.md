# Covida-backend

**DEPLOYMENT INITIAL SETUP**

**Clone and enter repo**  
```
git clone https://gitlab.com/kikobarretto/covida-backend.git
cd covida-backend
```



**Project Dependencies:**
*  Composer 1.10.1 (https://github.com/composer/composer)
*  Slim 4.0 (https://github.com/slimphp/Slim)
*  PHP 7.2 or newer
*  PSR7
*  PHP-DI
*  selective/config

**Check your PHP Version**  
`php -v`

**Install/Update PHP 7.3 (if needed)**  
`curl -s https://php-osx.liip.ch/install.sh | bash -s 7.3`

**Install PhP Composer**  
*This should be run inside your project folder*  
https://getcomposer.org/download/

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

**Make composer executable**  
*This should be run inside your project folder*  
`chmod +x composer.phar`

**Add Slim dependencies**  
*This should be run inside your project folder*  
```
./composer.phar require slim/slim:^4.0
./composer.phar require slim/psr7
./composer.phar require php-di/php-di
./composer.phar require selective/config
```

or
```
composer require slim/slim:^4.0
composer require slim/psr7
composer require php-di/php-di
composer require selective/config
```

**Check your composer.json.**  
*It should look like this. If not, copy and paste. Then run* `composer update`   
```
{
	"require": {
		"slim/slim": "^4.0",
		"slim/psr7": "^1.0",
		"php-di/php-di": "^6.1",
		"selective/config": "^0.2.0"
	},
	"autoload": {
		"psr-4": {
			"App\\": "src"
		}
	}
}
```

*In case of error "preg_match(): JIT compilation failed: no more memory" add the following line to php.ini (location of php.ini is indicated at php phpinfo(); )*  
`pcre.jit = 0`

**Test using built-in PHP server:**  
`$ php -S localhost:8000 -t public`

Access http://localhost:8000 to check if its running properly