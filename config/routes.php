<?php

use Slim\App;

return function (App $app) {
	$app->get('/', \App\Action\HomeAction::class);

	$app->put('/test/', \App\Action\InsertUserTestAction::class);

	$app->get('/test/{email}', \App\Action\GetLastUserTestAction::class);

	// $app->get('/user/{email}', function (ServerRequestInterface $request, ResponseInterface $response) {
	// 	$email = $args['email'];

	// 	$response->getBody()->write("Hello, $email");

	// // require_once('db.php');
	// // $connection = DBConnectionFactory::getConnection();
	// // $get_email = htmlspecialchars($request->getAttribute('email'));
	// // $query = "UPDATE library SET book_name = ?, book_isbn = ?, book_category = ? WHERE book_id = ?";
	// // $stmt = $connection->prepare($query);
	// // $stmt->bind_param("sss",$book_name,$book_isbn,$book_category);
	// // $book_name = $request->getParsedBody()['book_name'];
	// // $book_isbn = $request->getParsedBody()['book_isbn'];
	// // $book_category = $request->getParsedBody()['book_category'];
	// // $book_category = $request->getParsedBody()['book_id'];
	// // $stmt->execute();

	// // $stmt->close();
	// // DBConnectionFactory::closeConnection($conn);


	// 	// echo json_encode($email);
	// 	return $response;
	// });

};