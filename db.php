<?php

class DBConnectionFactory {

	private static $servername = "";
	private static $username = "";
	private static $password = "";
	private static $dbname = "";


	public static function getConnection() {
		self::loadConfig();

		$conn = new mysqli(self::$servername, self::$username, self::$password, self::$dbname);

		if ($conn->connect_error) {
			echo("Connection error");
		} 

		if (!$conn->set_charset("utf8")) {
			printf("Error loading character set utf8: %s\n", $conn->error);
		} 
		return $conn;
	}

	public static function close($conn) {
		$conn->close();
	}

	public static function loadConfig() {
		$jsonStr = file_get_contents("config.json", true);
		$database = json_decode($jsonStr); // if you put json_decode($jsonStr, true), it will convert the json string to associative array
		
		self::$servername = $database->host;
    	self::$username = $database->user;
    	self::$password = $database->password;
    	self::$dbname = $database->dbname;
	}
}

?>