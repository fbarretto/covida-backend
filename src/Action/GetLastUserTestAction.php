<?php
namespace App\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetLastUserTestAction
{
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
	{	
		$json["success"] = false;
		// $response->getBody()->write('<a href="/user/francisco">Try /user/francisco</a>');
		$email = htmlspecialchars((string)($request->getAttribute('email') ?? ''));
		if ($email!=''){
			$json["success"] = true;

			$SymptomData["hasFever"] = true;
			$SymptomData["hasAnotherRespSymptom"] = null;
			$SymptomData["hadConfirmedHomeContact"] = true;
			$SymptomData["hadCloseContactConfirmedOrSuspectedCase"] = false;
			$SymptomData["hadInternationalTravelReturnInTheLast14Days"] = false;
			$SymptomData["hasCaseClassification"] = true;
			$SymptomData["isSuspect"] = true;
			$SymptomData["isProbable"] = true;
			$SymptomData["isRejected"] = true;
			$SymptomData["hasDyspnoea"] = true;
			$SymptomData["hasComorbidity"] = true;
			$SymptomData["risk"] = true;
			

			$otherSymptomData["hasTosse"] = true;
			$otherSymptomData["hasDorGarganta"] = true;
			$otherSymptomData["DificuldadeDeRespirar"] = true;
			$otherSymptomData["hasMialgiaOrArtralgia"] = false;
			$otherSymptomData["hasNauseaOrVomitos"] = false;
			$otherSymptomData["hasCefaleia"] = false;
			$otherSymptomData["hasCoriza"] = false;
			$otherSymptomData["hasIrritabilidadeOrConfusao"] = false;
			$otherSymptomData["hasAdinamia"] = false;
			$otherSymptomData["hasProducaoDeEscarro"] = false;
			$otherSymptomData["hasCalafrios"] = false;
			$otherSymptomData["hasCongestaoNasal"] = false;
			$otherSymptomData["hasCongestaoConjuntival"] = false;
			$otherSymptomData["hasDificuldadeParaDeglutir"] = false;
			$otherSymptomData["hasManchasVermelhasPeloCorpo"] = false;
			
			$comorboditySymptomData["hasDoencaCardiaca"] = true;
			$comorboditySymptomData["hasDoencaPulmonar"] = null;
			$comorboditySymptomData["hasDiabetes"] = false;
			$comorboditySymptomData["hasInsuficenciaRenal"] = false;
			$comorboditySymptomData["hasOncologica"] = false;
			$comorboditySymptomData["hasOutraDoencaCronica"] = null;
			
			$medicinesData["useAntiinflamatorios"] = false;
			$medicinesData["useAntitermico"] = true;
			$medicinesData["useAnalgesico"] = false;
			$medicinesData["useImunossupressor"] = false;

			$userData["email"] = $email;
			$userData["age"] = 27;
			$userData["genero de nascimento"] = 'M';

			$userLocation["cep"] = "40110-170";
			$userLocation["logradouro"] = "Rua das Flores";
			$userLocation["complemento"] = "";
			$userLocation["bairro"] = "Asa Norte";
			$userLocation["localidade"] = "Brasília";
			$userLocation["uf"] = "DF";
			$userLocation["unidade"] = "";
			$userLocation["ibge"] = "2927408";
			$userLocation["gia"] = "";

			$complementaryUserData["Horas de sono"] = 7.5;
			$complementaryUserData["Exercícios"] = false;
			$complementaryUserData["Profissão"] = "Professor";



			$json["symptomData"] = $SymptomData;
			$json["otherSymptomData"] = $otherSymptomData;		
			$json["comorboditySymptomData"] = $comorboditySymptomData;
			$json["medicinesData"] = $medicinesData;
			$json["userData"] = $userData;
			$json["userLocation"] = $userLocation;
			$json["complementaryUserData"] = $complementaryUserData;


		} 

		$response->getBody()->write(json_encode($json));

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}
}