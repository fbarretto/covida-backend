<?php
namespace App\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class InsertUserTestAction
{
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
	{	
		$json["success"] = false;

		$data = (array)$request->getParsedBody();
		$email = (string)($data['email'] ?? '');
		

		if ($email != ''){
			$json["success"] = true;
		} else {
			$json["message"] = "Email not set";
		}

		$response->getBody()->write(json_encode($json));

		return $response->withHeader('Content-Type', 'application/json');
		// return $response;
	}
}