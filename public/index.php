<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

(require __DIR__ . '/../config/bootstrap.php')->run();

// // Instantiate App
// $app = AppFactory::create();

// // Add error middleware
// $app->addErrorMiddleware(true, true, true);

// // Add routes
// $app->get('/', function (Request $request, Response $response) {
// 	$response->getBody()->write('<a href="/user/francisco">Try /user/francisco</a>');
// 	return $response;
// });

// $app->get('/user/{email}', function (Request $request, Response $response, $args) {
// 	$email = $args['email'];
	
// 	$response->getBody()->write("Hello, $email");

// 	// require_once('db.php');
// 	// $connection = DBConnectionFactory::getConnection();
// 	// $get_email = htmlspecialchars($request->getAttribute('email'));
// 	// $query = "UPDATE library SET book_name = ?, book_isbn = ?, book_category = ? WHERE book_id = ?";
// 	// $stmt = $connection->prepare($query);
// 	// $stmt->bind_param("sss",$book_name,$book_isbn,$book_category);
// 	// $book_name = $request->getParsedBody()['book_name'];
// 	// $book_isbn = $request->getParsedBody()['book_isbn'];
// 	// $book_category = $request->getParsedBody()['book_category'];
// 	// $book_category = $request->getParsedBody()['book_id'];
// 	// $stmt->execute();

// 	// $stmt->close();
// 	// DBConnectionFactory::closeConnection($conn);


// 	echo json_encode($email);
// });

// $app->run();